package com.gladio.intrepid.vinoveritas.config;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.gladio.intrepid.vinoveritas.persistence.entity.ProductEntity;
import com.gladio.intrepid.vinoveritas.persistence.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.web.servlet.ServletRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.http.converter.json.Jackson2ObjectMapperBuilder;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.ViewControllerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

import javax.annotation.PostConstruct;
import javax.servlet.annotation.WebServlet;

/**
 * Created by gladio on 27/05/17.
 */
@org.springframework.context.annotation.Configuration
public class Configuration extends WebMvcConfigurerAdapter {

    @Autowired
    ProductRepository productRepository;

    @PostConstruct
    public void init() {

        productRepository.save(new ProductEntity("Franciacorta Millesimato", 32L, "app/img/franciacorta"));
        productRepository.save(new ProductEntity("Barolo Riserva 2009", 40L, "app/img/barolo"));
        productRepository.save(new ProductEntity("Grillo Tasca d'Almerita", 22L, "app/img/grillo"));
        productRepository.save(new ProductEntity("Barbera d’Asti", 29L, "app/img/barbera"));

    }

    @Override
    public void addViewControllers(ViewControllerRegistry registry) {
        registry.addViewController("/").setViewName("forward:/index.html");
    }

}
