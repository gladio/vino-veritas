package com.gladio.intrepid.vinoveritas.controller;

import com.gladio.intrepid.vinoveritas.persistence.entity.ProductEntity;
import com.gladio.intrepid.vinoveritas.persistence.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.Collections;
import java.util.List;

/**
 * Created by gladio on 27/05/17.
 */
@Controller
public class ProductController {

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(value = "/product", method = RequestMethod.GET )
    @ResponseBody
    public List<ProductEntity> getAll() {
        return productRepository.findAll();
    }

    @RequestMapping(value = "/product/{id}", method = RequestMethod.GET )
    @ResponseBody
    public ProductEntity getAll(@PathVariable Long id) {
        return productRepository.findOne(id);
    }

}
