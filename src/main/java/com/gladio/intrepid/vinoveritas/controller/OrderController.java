package com.gladio.intrepid.vinoveritas.controller;

import com.gladio.intrepid.vinoveritas.persistence.entity.OrderEntity;
import com.gladio.intrepid.vinoveritas.persistence.entity.ProductEntity;
import com.gladio.intrepid.vinoveritas.persistence.repository.OrderRepository;
import com.gladio.intrepid.vinoveritas.persistence.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * Created by gladio on 27/05/17.
 */
@Controller
public class OrderController {

    @Autowired
    OrderRepository orderRepository;

    @Autowired
    ProductRepository productRepository;

    @RequestMapping(value = "/order", method = RequestMethod.POST)
    @ResponseBody
    public void order(@RequestBody OrderEntity order) {
        orderRepository.save(order);
    }

    @RequestMapping(value = "/order", method = RequestMethod.GET)
    @ResponseBody
    public List<OrderEntity> order() {
        return orderRepository.findAll();
    }


}
