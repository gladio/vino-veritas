package com.gladio.intrepid.vinoveritas.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by gladio on 29/08/16.
 */
@Entity
@Table(name = "orders")
public class OrderEntity implements Serializable{

    @Id
    @Column
    @SequenceGenerator(name = "orders_gen", sequenceName = "S_orders_gen")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "orders_gen")
    private Long id;

    @Basic
    @Column
    private String customerName;

    @Basic
    @Column
    private String customerSurname;

    @Basic
    @Column
    private String productId;

    @Basic
    @Column
    private String address;
    @Basic
    @Column
    private String city;

    @Basic
    @Column
    private Integer quantity;
    @Basic
    @Column
    private String email;

    @Basic
    @Column
    private Integer totalPrice;

    @Basic
    @Column
    private String productName;

    @Temporal(TemporalType.DATE)
    @Column
    private Date birthDay;

    public OrderEntity() {
    }

    public OrderEntity(String customerName, String customerSurname, String productId, String address, String city, Integer quantity, String email, Integer totalPrice, String productName, Date birthDay) {
        this.customerName = customerName;
        this.customerSurname = customerSurname;
        this.productId = productId;
        this.address = address;
        this.city = city;
        this.quantity = quantity;
        this.email = email;
        this.totalPrice = totalPrice;
        this.productName = productName;
        this.birthDay = birthDay;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getCustomerName() {
        return customerName;
    }

    public void setCustomerName(String customerName) {
        this.customerName = customerName;
    }

    public String getCustomerSurname() {
        return customerSurname;
    }

    public void setCustomerSurname(String customerSurname) {
        this.customerSurname = customerSurname;
    }

    public String getProductId() {
        return productId;
    }

    public void setProductId(String productId) {
        this.productId = productId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public Integer getQuantity() {
        return quantity;
    }

    public void setQuantity(Integer quantity) {
        this.quantity = quantity;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Integer getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(Integer totalPrice) {
        this.totalPrice = totalPrice;
    }

    public Date getBirthDay() {
        return birthDay;
    }

    public void setBirthDay(Date birthDay) {
        this.birthDay = birthDay;
    }
}
