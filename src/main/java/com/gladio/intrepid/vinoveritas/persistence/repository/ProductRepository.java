package com.gladio.intrepid.vinoveritas.persistence.repository;

import com.gladio.intrepid.vinoveritas.persistence.entity.ProductEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductRepository extends CrudRepository<ProductEntity, Long> {

    List<ProductEntity> findAll();
}