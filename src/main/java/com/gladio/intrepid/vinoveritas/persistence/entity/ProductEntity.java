package com.gladio.intrepid.vinoveritas.persistence.entity;

import javax.persistence.*;
import java.io.Serializable;
import java.util.LongSummaryStatistics;

/**
 * Created by gladio on 29/08/16.
 */
@Entity
@Table(name = "product")
public class ProductEntity implements Serializable{

    @Id
    @Column
    @SequenceGenerator(name = "product_gen", sequenceName = "S_product_gen")
    @GeneratedValue(strategy = GenerationType.AUTO, generator = "product_gen")
    private Long id;

    @Basic
    @Column
    private String productName;

    @Basic
    @Column
    private Long price;

    @Basic
    @Column
    private String imageUrl;

    public ProductEntity() {
    }

    public ProductEntity(String productName, Long price, String imageUrl) {
        this.productName = productName;
        this.price = price;
        this.imageUrl = imageUrl;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getProductName() {
        return productName;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
