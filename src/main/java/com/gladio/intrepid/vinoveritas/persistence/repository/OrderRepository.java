package com.gladio.intrepid.vinoveritas.persistence.repository;

import com.gladio.intrepid.vinoveritas.persistence.entity.OrderEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface OrderRepository extends CrudRepository<OrderEntity, Long> {

    List<OrderEntity> findAll();
}