define(['app', '../service/OrderService'], function (app) {
    app.controller("ordersController",
        function ($scope, orderService) {
            $scope.orders = orderService.getAll();
        })

});