define(['app'], function (app) {
    app.factory('orderService', function ($resource) {
        return $resource('/order', {}, {getAll: {method: 'GET', isArray :true}})
    })
});