define(['app'], function (app) {
    app.factory('productService', function ($resource) {
        return $resource('/product/:id', {id: "@id"}, {
            getAll: {method: 'GET', isArray :true},
            getById: { method: "GET", params: { id: 0 } }
        })
    })
});