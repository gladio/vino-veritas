define(['app', '../service/ProductService'], function (app) {
    app.controller("productsController",
        function ($scope, productService, $compile) {


            var entry = productService.getAll
            ().$promise.then(    //success
                function (value) {
                    console.log("Data from server are successfully retrieved! ")
                    console.log(value);
                    $scope.products = value;
                },
                //error
                function (error) {
                    console.log("Failed to retrieve data from server, using stubbed version!")
                    console.log(error);

                    $scope.products = [{
                        id: 1,
                        productName: 'Franciacorta Millesimato',
                        price: 32,
                        imageUrl: 'app/img/franciacorta'
                    }, {
                        id: 2,
                        productName: 'Barolo Riserva 2009',
                        price: 40,
                        imageUrl: 'app/img/barolo'
                    }, {
                        id: 3,
                        productName: "Grillo Tasca d'Almerita",
                        price: 22,
                        imageUrl: 'app/img/grillo'
                    }, {
                        id: 4,
                        productName: "Barbera d’Asti",
                        price: 29,
                        imageUrl: 'app/img/barbera'
                    }];
                });
        })

});