require.config({
    baseUrl: "app",
    paths: {
        'angular': '../bower_components/angular/angular.min',
        'angular-route': '../bower_components/angular-route/angular-route.min',
        'angular-bootstrap': 'http://angular-ui.github.io/bootstrap/ui-bootstrap-tpls-1.1.1.min',
        'angular-resource': '../bower_components/angular-resource/angular-resource.min',
        'angular-animate': '../bower_components/angular-animate/angular-animate.min',
        'jquery': '../bower_components/jquery/dist/jquery.min',
        'bootstrap': '../bower_components/bootstrap-css/js/bootstrap.min',
        'angularAMD': '../bower_components/angularAMD/angularAMD.min'
    },
    shim: { 'angularAMD': ['angular'],
        'angular-route': ['angular'],
        'angular-bootstrap': ['angular'],
        'angular-animate': ['angular'],
        'angular-resource': ['angular'],
        'bootstrap' : { deps :['jquery'] }
    },
    deps: ['app']
});
