define(['angularAMD', 'angular-route','angular-resource','angular-bootstrap','bootstrap'], function (angularAMD) {
    var app = angular.module("lemonApp", ['ngRoute', 'ui.bootstrap', 'ngResource']);
        app.config(['$httpProvider', function ($httpProvider) {
            $httpProvider.defaults.withCredentials = true;
        }]);

    app.config(function ($routeProvider) {
        $routeProvider
            .when("/home", angularAMD.route({
                templateUrl: 'app/view/home.html', controller: 'productsController', controllerUrl: 'controller/ProductsController'
            })) .when("/detail/:id", angularAMD.route({
                templateUrl: 'app/view/product-detail.html', controller: 'productDetailsController', controllerUrl: 'controller/ProductDetailsController'
            })) .when("/orders", angularAMD.route({
                templateUrl: 'app/view/orders.html', controller: 'ordersController', controllerUrl: 'controller/OrdersController'
            }))
            .otherwise({redirectTo: "/home"});
    });

    return angularAMD.bootstrap(app);
});